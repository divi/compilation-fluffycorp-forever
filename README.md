# Compilation FluffyCorp Forever

## Introduction 

La FluffyCorp est fière de vous présenter son compilateur/interpreteur pour le langage NilNovi algorithmique. Edition 2020  
Pour les détailes sur l'implémentation voir le [wiki](https://gitlab.enssat.fr/divi/compilation-fluffycorp-forever/-/wikis/README-:-organisation-du-wiki).

## Lancer la VM

### Linux & Windows

$ python run.py [d] [vm|as] [n]

* python : Script utilisant python3
* run.py : Script de test
* d      : Ce parametre permet de supprimer le log au lancement du script
* vm     : Ce parametre permet de lance la VM, c'est à dire l'execution d'un fichier .as
* as     : Ce parametre permet de lancer le processus entier : compilation plus execution d'un fichier .nno
* n      : Ce parametre précise l'id du fichier à lancer
	
Exemples :

$ python run.py vm 1

* Cette commande executera le fichier "./tests/assembleur/assembly1.as".
    
$ python run.py d as 2

* Cette commande supprimera les logs puis compilera le fichier "./tests/nna/correct2.syn" dans le fichier de sortie "./tests/out/correct2.syn", et enfin, executera ce fichier de sortie.

## Membres de L'équipe

- Tony Chouteau
- Divi De Lacour
- Marc Vacquant
- Simon Lerin
- Thibaut Gaudier
