import re
import string
import logging
import colorama

import src.tab as tab
import src.method as method

colorama.init()
ALPHA = string.ascii_letters

class vm:
	"""
		Method class definition : used to make main object
	"""
	
	exe = True

	##Constructors##
	def __init__(self):
		"""
			Constructor of the method class
		"""
		self.source = None
		self.output = None
		self.tab = tab.tab(self)
		self.pile = []
		self.method = method.method(None, None, self)

	##Setters & Getters##
	#setSource#
	#setSourceManual#
	#getSource#
	#setOutput#
	#setOutputManual#
	#getOutput#

	def setSource(self, source):
		"""
			Method to redefine the object variable source
		"""
		self.source = source

	def setSourceManual(self):
		"""
			Method to redefine the object variable source manually
		"""
		print("\033[32m" + "Source setting (manual) : starting" + "\033[39m")
		self.source = input()
		print("\033[32m" + "Source setting (manual) : done" + "\033[39m")

	def getSource(self):
		"""
			Method to get the object variable source
		"""
		print(self.source)

	def setOutput(self, output):
		"""
			Method to redefine the object variable output
		"""
		self.output = output

	def setOutputManual(self):
		"""
			Method to redefine the object variable output manually
		"""
		print("\033[32m" + "Output setting (manual) : starting" + "\033[39m")
		self.output = input()
		print("\033[32m" + "Output setting (manual) : done " + "\033[39m")

	def getOutput(self):
		"""
			Method to get the object variable output
		"""
		print(self.output)

	##Methods##
	#compileCode#
	#exeCode#

	def compileCode(self):
		"""
			compileCode is a method that read the pointed file of the current object and transform it in an interpretable form for the exeCode method
		"""
		logging.info("Source reading: starting" + "\nUsing source: " +
					 self.source + "\nUsing output: " + self.output)
		try:
			with open(self.source, 'r') as s, open(self.output, 'w') as w:
				sr = s.readlines()
				prog = False
				i = 0
				for x in sr:
					i += 1
					x = x.rstrip("\n")
					#lines starting with non-alphabetic character are not processed#
					if (x.startswith(tuple(ALPHA))):
						name = re.sub("[^a-zA-Z]+", "", x)
						arg = re.sub(r"\D", "", x)
						f = (name, arg)
						#detection of code start#
						if (f[0] == "debutProg" and prog == False):
							prog = True
							self.tab.addContent(f)
							w.write(x+"\n")
						#detection of debutProg non-unicity#
						elif (f[0] == "debutProg" and prog == True):
							logging.error("\"debutProg\" not unique")
							
						#detection of code end#
						elif (f[0] == "finProg" and prog == True):
							self.tab.addContent(f)
							w.write(x+"\n")
							break
						#detection of finProg before debutProg#
						elif (f[0] == "finProg" and prog == False):
							logging.error("\"finProg\" before debutProg")
							
						#writting of current line#
						elif(prog == True):
							if (self.tab.addContent(f)):
								w.write(f[0] + "(" + f[1] + ")" + "\n")
						#no processing for unrecognized line however the line is stored in logs#
						else:
							logging.info("unknown line : " + f)
		except:
			logging.error("Compilation failed")
			raise
		s.close
		w.close

	def exeCode(self):
		"""
			exeCode perform a sequence of instructions translated by compileCode from the pointed file of the current object
		"""
		while (self.exe == True):
			self.tab.runInstruction(self.tab.pointer)