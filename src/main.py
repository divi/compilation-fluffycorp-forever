# -*-coding:Latin-1 -*
#!/usr/bin/python3

#====================================================
# Import
#====================================================


#======================
# Vendors

from pathlib import Path
import os
import sys
import re
import datetime
import logging

#======================
# Own

#import method
import src.vm as vm
#import tab

#====================================================
# Functions
#====================================================

def start(source):
	#log creation
	truetime = str(datetime.datetime.now())
	time = re.sub(r"\D", "_", truetime.split(".")[0])
	outputPath = "./log/" + time + "/"
	Path(outputPath).mkdir(parents=True, exist_ok=True)
	logging.basicConfig(filename=outputPath + "vm_" +
						time + ".log", level=logging.INFO)
	logging.info("Started at " + truetime)

	#files' paths treatment
	try:
		#objects'creation
		try:
			print("\nStarting the VM")
			VM = vm.vm()
		except:
			logging.error("VM Creation Error")
			raise

		#source file's path
		try:
			print("Getting the file")
			#sourcePath = sys.argv[1]
			sourcePath = source
			# Allows errors with "/" and "\"
			sourcePath = re.sub("\\\\", "/", sourcePath)
			f = open(sourcePath)
			VM.setSource(sourcePath)
		except IOError:
			logging.error("Source Path Error")
			raise
		finally:
			f.close()

		#output file's path
		try:
			VM.setOutput(outputPath + "/tab" + "_" + time + ".log")
		except IOError:
			logging.error("Output Path Error")
			raise
		finally:
			f.close()

		#compilation
		try:
			print("Compilation started")
			VM.compileCode()
		except:
			logging.error("Compilation failed")
			raise

		#wait before execution
		try:
			if (sys.platform == "linux"):
				input("\nCompilation done, press enter to run\033[39m\n")
			else:
				input("\nCompilation done, press enter to run\n")
		except SyntaxError:
			pass

		# Exécution
		try:
			VM.exeCode()
		except:
			logging.error("Execution failed")
			raise

	#error alert
	except:
		logging.error("FATAL ERROR")
		print("\033[31m" + "ERROR, REFER TO LOG" + "\033[39m")

	# Fin du log
	logging.info("Finished at " + str(datetime.datetime.now()))

#====================================================
# Entry Point
#====================================================

if __name__ == "__main__":
	start(sys.argv[1])

#====================================================
# End
#====================================================