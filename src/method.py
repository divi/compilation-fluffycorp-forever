#====================================================
# Import
#====================================================

#======================
# Vendors

#

#======================
# Own

import inspect
import logging

#====================================================
# Class Definition
#====================================================

class method:
	"""
		Method class definition : used to make basic operations
	"""

	#Constructor
	def __init__(self, name, argument, VM):
		"""
			Constructor of the method class
		"""
		self.name = name
		self.argument = argument
		self.VM = VM

	##S&G##
	def setValue(self, value):
		"""
			Method to redefine the object variables
		"""
		self.name = value[0]
		self.argument = value[1]

	##Methods##
	def isValide(self):
		"""
			Check if the line can be well interpreted
		"""
		if self.name in dir(method):
			return True
		else:
			logging.error(self.name + " is unknown")
			return False

	def debutProg(self):
		"""
			Set the start of a program
		"""
		print("Hello, here is the Fluffy-san assistant, you programm starts now ^^ :\n\n============")
		self.VM.pile.append(0)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def finProg(self):
		"""
			Set the end of a program
		"""
		print("============\n\nYour program ran successfully\n")
		self.VM.exe = False

	def put(self):
		"""
			Put an element in the stack 
		"""
		sommet = self.VM.pile.pop()
		print("j'affiche ", sommet)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def empiler(self, int):
		"""
			Stack integer in the stack
		"""
		print(self.VM.pile)
		print("empiler")
		self.VM.pile.append(int)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def affectation(self):
		"""
			Affectation of en element of the pile at it's address 
		"""
		print(self.VM.pile)
		print("affectation")
		valeurSommet = self.VM.pile.pop()
		adresse = self.VM.pile.pop()
		self.VM.pile[adresse] = valeurSommet
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def sous(self):
		"""
			Subtract 2 elements of the stack
		"""
		print(self.VM.pile)
		print("sous")
		valeurSommet = self.VM.pile.pop()
		self.VM.pile[-1] = self.VM.pile[-1]-valeurSommet
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def add(self):
		"""
			Add 2 elements of the stack
		"""
		print(self.VM.pile)
		print("add")
		valeurSommet = self.VM.pile.pop()
		self.VM.pile[-1] += valeurSommet
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)


	def mult(self):
		"""
			Multiply 2 elements of the stack
		"""
		print(self.VM.pile)
		print("mult")
		valeurSommet = self.VM.pile.pop()
		self.VM.pile[-1] = self.VM.pile[-1] * valeurSommet
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def div(self):
		"""
			Divide 2 elements of the stack
		"""
		print(self.VM.pile)
		print("div")
		valeurSommet = self.VM.pile.pop()
		self.VM.pile[-1] = self.VM.pile[-1] // valeurSommet
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def reserver(self, n):
		"""
			Multiply 2 elements of the stack
		"""
		print(self.VM.pile)
		print("reserver")
		for k in range(n):
			self.VM.pile.append(0)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)
	
	def empileAd(self, add):
		"""
			Stack at a specified address
		"""
		print(self.VM.pile)
		print("empileAd")
		self.VM.pile[-1] = self.VM.pile[add]
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def get(self):
		"""
			Get the first element of the stack
		"""
		print(self.VM.pile)
		print("get")
		n = input()
		if (n == ""):
			n = 0
		n = int(n)
		print("n= ",n)
		sommet = self.VM.pile.pop()
		self.VM.pile[sommet] = n
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)


	def moins(self):
		"""
			Change the value to its opposite
		"""
		print(self.VM.pile)
		print("moins")
		self.VM.pile[-1] = -(self.VM.pile[-1])
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)
	
	def egal(self):
		"""
			Compare values of the stack (true if equal)
		"""
		print(self.VM.pile)
		print("egal")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 == val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)
	
	def diff(self):
		"""
			Compare values of the stack (true if different)
		"""
		print(self.VM.pile)
		print("diff")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if not val1==val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def inf(self):
		"""
			Compare values of the stack (true if inferior)
		"""
		print(self.VM.pile)
		print("inf")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 > val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def infeg(self):
		"""
			Compare values of the stack (true if inferior or equal)
		"""
		print(self.VM.pile)
		print("infeg")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 >= val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def sup(self):
		"""
			Compare values of the stack (true if superior)
		"""
		print(self.VM.pile)
		print("sup")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 < val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def supeg(self):
		"""
			Compare values of the stack (true if superior or equal)
		"""
		print(self.VM.pile)
		print("supeg")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 <= val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def et(self):
		"""
			Do a logical "and" operation between 2 values
		"""
		print(self.VM.pile)
		print("et")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 and val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def ou(self):
		"""
			Do a logical "or" operation between 2 values
		"""
		print(self.VM.pile)
		print("ou")
		val1 = self.VM.pile.pop()
		val2 = self.VM.pile.pop()
		if val1 or val2:
			self.VM.pile.append(True)
		else:
			self.VM.pile.append(False)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def non(self):
		"""
			Do a logical "or" operation on the last value of the stack
		"""
		print(self.VM.pile)
		print("non")
		val1 = self.VM.pile.pop()
		if val1:
			self.VM.pile.append(False)
		else:
			self.VM.pile.append(True)
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def valeurPile(self):
		"""
			Get the value at the address in the stack
		"""
		print(self.VM.pile)
		print("valpile")
		adSommet = int(self.VM.pile[-1])
		self.VM.pile[-1] = self.VM.pile[adSommet]
		self.VM.tab.setPointer(self.VM.tab.pointer + 1)

	def tra(self, ad):
		"""
			Do a "tra" opperation
		"""
		print(self.VM.pile)
		print("tra")
		self.VM.tab.setPointer(ad)

	def tze(self, ad):
		"""
			Do a "tze" opperation
		"""
		print(self.VM.pile)
		print("tze")
		valSommet = self.VM.pile.pop()
		if not valSommet:
			self.VM.tab.setPointer(ad)
		else:
			self.VM.tab.setPointer(self.VM.tab.pointer + 1)

#====================================================
# End
#====================================================