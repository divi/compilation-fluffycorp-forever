#!/usr/bin/python

## 	@package anasyn
# 	Syntactical Analyser package. 
#

import sys, argparse, re
import logging

import src.analex as an

import src.main as m

logger = logging.getLogger('anasyn')

DEBUG = False
LOGGING_LEVEL = logging.DEBUG

#% Table des identificateurs globales, car on ne trouve pas comment faire autrement
identifierTable = {}

#% Table du code assembleur à output, chaque élément est en ['nominstruct', Integer ou NULL]
instructionTable = []

#% Table des opérations en à traiter
operationTable = []

#Pile adresses pour boucles et conditions
pileBC = []

class AnaSynException(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
				return repr(self.value)

########################################################################				 	
#### Syntactical Diagrams
########################################################################				 	

def program(lexical_analyser):
	specifProgPrinc(lexical_analyser)
	lexical_analyser.acceptKeyword("is")
	corpsProgPrinc(lexical_analyser)
	
def specifProgPrinc(lexical_analyser):
	lexical_analyser.acceptKeyword("procedure")
	ident = lexical_analyser.acceptIdentifier()
	logger.debug("Name of program : "+ident)
	##! On ajoute la procédure à la table des identificateurs
	identifierTable[ident] = ["procedure", len(identifierTable), []]

def  corpsProgPrinc(lexical_analyser):
	if not lexical_analyser.isKeyword("begin"):
		logger.debug("Parsing declarations")
		instructionTable.append("debutProg") #%
		partieDecla(lexical_analyser)
		logger.debug("End of declarations")
		#% On réserve dans la pile un espace correspondant au nombre d'identificateurs
		instructionTable.append("reserver("+str(len(list(identifierTable.keys())))+ ")")

	lexical_analyser.acceptKeyword("begin")

	if not lexical_analyser.isKeyword("end"):
		logger.debug("Parsing instructions")
		suiteInstr(lexical_analyser)
		logger.debug("End of instructions")
		#% finProg
		instructionTable.append("finProg")
			
	lexical_analyser.acceptKeyword("end")
	lexical_analyser.acceptFel()
	logger.debug("End of program")
	
def partieDecla(lexical_analyser):
		if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function") :
				listeDeclaOp(lexical_analyser)
				if not lexical_analyser.isKeyword("begin"):
						listeDeclaVar(lexical_analyser)
		
		else:
				listeDeclaVar(lexical_analyser)				

def listeDeclaOp(lexical_analyser):
	declaOp(lexical_analyser)
	lexical_analyser.acceptCharacter(";")
	if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword("function") :
		listeDeclaOp(lexical_analyser)

def declaOp(lexical_analyser):
	if lexical_analyser.isKeyword("procedure"):
		procedure(lexical_analyser)
	if lexical_analyser.isKeyword("function"):
		fonction(lexical_analyser)

def procedure(lexical_analyser):
	lexical_analyser.acceptKeyword("procedure")
	ident = lexical_analyser.acceptIdentifier()
	logger.debug("Name of procedure : "+ident)
	##! Ajoute une procedure à la table des identificateurs
	identifierTable[ident] = ["procedure", len(identifierTable), []] 
	partieFormelle(lexical_analyser)

	lexical_analyser.acceptKeyword("is")
	corpsProc(lexical_analyser)
	   

def fonction(lexical_analyser):
	lexical_analyser.acceptKeyword("function")
	ident = lexical_analyser.acceptIdentifier()
	logger.debug("Name of function : "+ident)
	##! Ajoute une fonction à la table des identificateurs
	identifierTable[ident] = ["function", len(identifierTable), [], "integer"]

	partieFormelle(lexical_analyser)

	lexical_analyser.acceptKeyword("return")
	nnpType(lexical_analyser)
		
	lexical_analyser.acceptKeyword("is")
	corpsFonct(lexical_analyser)

def corpsProc(lexical_analyser):
	if not lexical_analyser.isKeyword("begin"):
		partieDeclaProc(lexical_analyser)
	lexical_analyser.acceptKeyword("begin")
	suiteInstr(lexical_analyser)
	lexical_analyser.acceptKeyword("end")

def corpsFonct(lexical_analyser):
	if not lexical_analyser.isKeyword("begin"):
		partieDeclaProc(lexical_analyser)
	lexical_analyser.acceptKeyword("begin")
	suiteInstrNonVide(lexical_analyser)
	lexical_analyser.acceptKeyword("end")

def partieFormelle(lexical_analyser):
	lexical_analyser.acceptCharacter("(")
	if not lexical_analyser.isCharacter(")"):
		listeSpecifFormelles(lexical_analyser)
	lexical_analyser.acceptCharacter(")")

def listeSpecifFormelles(lexical_analyser):
	specif(lexical_analyser)
	if not lexical_analyser.isCharacter(")"):
		lexical_analyser.acceptCharacter(";")
		listeSpecifFormelles(lexical_analyser)

def specif(lexical_analyser):
	listeIdent(lexical_analyser)
	lexical_analyser.acceptCharacter(":")
	if lexical_analyser.isKeyword("in"):
		mode(lexical_analyser)
				
	nnpType(lexical_analyser)

def mode(lexical_analyser):
	lexical_analyser.acceptKeyword("in")
	if lexical_analyser.isKeyword("out"):
		lexical_analyser.acceptKeyword("out")
		logger.debug("in out parameter")				
	else:
		logger.debug("in parameter")

def nnpType(lexical_analyser):
	if lexical_analyser.isKeyword("integer"):
		lexical_analyser.acceptKeyword("integer")
		logger.debug("integer type")
		for i in list(identifierTable.keys()):##! Met toutes les variables dont on ne connaissait pas le type en integer
			if identifierTable[i][0] == "tmp":
				identifierTable[i][0] = "integer"
	elif lexical_analyser.isKeyword("boolean"):
		lexical_analyser.acceptKeyword("boolean")
		logger.debug("boolean type")  
		for i in list(identifierTable.keys()):##! Met toutes les variables dont on ne connaissait pas le type en booleen
			if identifierTable[i][0] == "tmp":
				identifierTable[i][0] = "boolean"
	else:
		logger.error("Unknown type found <"+ lexical_analyser.get_value() +">!")
		raise AnaSynException("Unknown type found <"+ lexical_analyser.get_value() +">!")

def partieDeclaProc(lexical_analyser):
	listeDeclaVar(lexical_analyser)

def listeDeclaVar(lexical_analyser):
	declaVar(lexical_analyser)
	if lexical_analyser.isIdentifier():
		listeDeclaVar(lexical_analyser)

def declaVar(lexical_analyser):
	listeIdent(lexical_analyser)
	lexical_analyser.acceptCharacter(":")
	logger.debug("now parsing type...")
	nnpType(lexical_analyser)
	lexical_analyser.acceptCharacter(";")

def listeIdent(lexical_analyser):
	ident = lexical_analyser.acceptIdentifier()
	logger.debug("identifier found: "+str(ident))
	##! Ajoute une variable à la table des identificateurs
	##! Met tmp en type en attendant de lire le type
	identifierTable[ident] = ["tmp", len(identifierTable)]

	if lexical_analyser.isCharacter(","):
		lexical_analyser.acceptCharacter(",")
		listeIdent(lexical_analyser)

def suiteInstrNonVide(lexical_analyser):
	instr(lexical_analyser)
	if lexical_analyser.isCharacter(";"):
		lexical_analyser.acceptCharacter(";")
		suiteInstrNonVide(lexical_analyser)

def suiteInstr(lexical_analyser):
	if not lexical_analyser.isKeyword("end"):
		suiteInstrNonVide(lexical_analyser)

def instr(lexical_analyser):		
	if lexical_analyser.isKeyword("while"):
		boucle(lexical_analyser)
	elif lexical_analyser.isKeyword("if"):
		altern(lexical_analyser)
	elif lexical_analyser.isKeyword("get") or lexical_analyser.isKeyword("put"):
		es(lexical_analyser)
	elif lexical_analyser.isKeyword("return"):
		retour(lexical_analyser)
	elif lexical_analyser.isIdentifier():
		ident = lexical_analyser.acceptIdentifier()
		instructionTable.append("empiler("+ str(identifierTable[ident][1])+")")
		if lexical_analyser.isSymbol(":="):				
			lexical_analyser.acceptSymbol(":=")
			expression(lexical_analyser)
			logger.debug("parsed affectation")
			#% affectation
			instructionTable.append("affectation")
		elif lexical_analyser.isCharacter("("):
			lexical_analyser.acceptCharacter("(")
			if not lexical_analyser.isCharacter(")"):
				listePe(lexical_analyser)

			lexical_analyser.acceptCharacter(")")
			logger.debug("parsed procedure call")
		else:
			logger.error("Expecting procedure call or affectation!")
			raise AnaSynException("Expecting procedure call or affectation!")
		
	else:
		logger.error("Unknown Instruction <"+ lexical_analyser.get_value() +">!")
		raise AnaSynException("Unknown Instruction <"+ lexical_analyser.get_value() +">!")

def listePe(lexical_analyser):
	expression(lexical_analyser)
	if lexical_analyser.isCharacter(","):
		lexical_analyser.acceptCharacter(",")
		listePe(lexical_analyser)

def expression(lexical_analyser):
	logger.debug("parsing expression: " + str(lexical_analyser.get_value()))

	exp1(lexical_analyser)
	if lexical_analyser.isKeyword("or"):
		lexical_analyser.acceptKeyword("or")
		operationTable.append("ou()")
		exp1(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)
        
def exp1(lexical_analyser):
	logger.debug("parsing exp1")
	
	exp2(lexical_analyser)
	if lexical_analyser.isKeyword("and"):
		lexical_analyser.acceptKeyword("and")
		operationTable.append("et()")
		exp2(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)
        
def exp2(lexical_analyser):
	logger.debug("parsing exp2")
		
	exp3(lexical_analyser)
	if	lexical_analyser.isSymbol("<") or \
		lexical_analyser.isSymbol("<=") or \
		lexical_analyser.isSymbol(">") or \
		lexical_analyser.isSymbol(">="):
		opRel(lexical_analyser)
		exp3(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)
	elif lexical_analyser.isSymbol("=") or \
		lexical_analyser.isSymbol("/="): 
		opRel(lexical_analyser)
		exp3(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)
	
def opRel(lexical_analyser):
	logger.debug("parsing relationnal operator: " + lexical_analyser.get_value())
		
	if	lexical_analyser.isSymbol("<"):
		lexical_analyser.acceptSymbol("<")
		operationTable.append("inf()")
        
	elif lexical_analyser.isSymbol("<="):
		lexical_analyser.acceptSymbol("<=")
		operationTable.append("infeg()")
        
	elif lexical_analyser.isSymbol(">"):
		lexical_analyser.acceptSymbol(">")
		operationTable.append("sup()")
        
	elif lexical_analyser.isSymbol(">="):
		lexical_analyser.acceptSymbol(">=")
		operationTable.append("supeg()")
        
	elif lexical_analyser.isSymbol("="):
		lexical_analyser.acceptSymbol("=")
		operationTable.append("egal()")
        
	elif lexical_analyser.isSymbol("/="):
		lexical_analyser.acceptSymbol("/=")
		operationTable.append("diff()")
        
	else:
		msg = "Unknown relationnal operator <"+ lexical_analyser.get_value() +">!"
		logger.error(msg)
		raise AnaSynException(msg)

def exp3(lexical_analyser):
	logger.debug("parsing exp3")
	exp4(lexical_analyser)	
	if lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-"):
		opAdd(lexical_analyser)
		exp4(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)

def opAdd(lexical_analyser):
	logger.debug("parsing additive operator: " + lexical_analyser.get_value())
	if lexical_analyser.isCharacter("+"):
		lexical_analyser.acceptCharacter("+")
		operationTable.append("add()")
                
	elif lexical_analyser.isCharacter("-"):
		lexical_analyser.acceptCharacter("-")
		operationTable.append("sous()")
                
	else:
		msg = "Unknown additive operator <"+ lexical_analyser.get_value() +">!"
		logger.error(msg)
		raise AnaSynException(msg)

def exp4(lexical_analyser):
	logger.debug("parsing exp4")
		
	prim(lexical_analyser)	
	if lexical_analyser.isCharacter("*") or lexical_analyser.isCharacter("/"):
		opMult(lexical_analyser)
		prim(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)

def opMult(lexical_analyser):
	logger.debug("parsing multiplicative operator: " + lexical_analyser.get_value())
	if lexical_analyser.isCharacter("*"):
		lexical_analyser.acceptCharacter("*")
		operationTable.append("mult()")
                
	elif lexical_analyser.isCharacter("/"):
		lexical_analyser.acceptCharacter("/")
		operationTable.append("div()")
                
	else:
		msg = "Unknown multiplicative operator <"+ lexical_analyser.get_value() +">!"
		logger.error(msg)
		raise AnaSynException(msg)

def prim(lexical_analyser):
	logger.debug("parsing prim")
#%       
	if lexical_analyser.isCharacter("-") or lexical_analyser.isKeyword("not"):
		opUnaire(lexical_analyser)
		elemPrim(lexical_analyser)
		temp_str = operationTable.pop()
		instructionTable.append(temp_str)
	elif lexical_analyser.isCharacter("+"):
		opUnaire(lexical_analyser)
		elemPrim(lexical_analyser)
	else :
		elemPrim(lexical_analyser)
#%

def opUnaire(lexical_analyser):
	logger.debug("parsing unary operator: " + lexical_analyser.get_value())
	if lexical_analyser.isCharacter("+"):
		lexical_analyser.acceptCharacter("+")
		#% rien
				
	elif lexical_analyser.isCharacter("-"):
		lexical_analyser.acceptCharacter("-")
		operationTable.append("moins()")
		
				
	elif lexical_analyser.isKeyword("not"):
		lexical_analyser.acceptKeyword("not")
		operationTable.append("non()")
                
	else:
		msg = "Unknown additive operator <"+ lexical_analyser.get_value() +">!"
		logger.error(msg)
		raise AnaSynException(msg)

def elemPrim(lexical_analyser):
	logger.debug("parsing elemPrim: " + str(lexical_analyser.get_value()))
	if lexical_analyser.isCharacter("("):
		lexical_analyser.acceptCharacter("(")
		expression(lexical_analyser)
		lexical_analyser.acceptCharacter(")")
	elif lexical_analyser.isInteger() or lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
		valeur(lexical_analyser)
	elif lexical_analyser.isIdentifier():
		ident = lexical_analyser.acceptIdentifier()
		if lexical_analyser.isCharacter("("):			# Appel fonct
			lexical_analyser.acceptCharacter("(")
			if not lexical_analyser.isCharacter(")"):
				listePe(lexical_analyser)

			lexical_analyser.acceptCharacter(")")
			#% 
			logger.debug("parsed procedure call")

			logger.debug("Call to function: " + ident)
		else:
			logger.debug("Use of an identifier as an expression: " + ident)
			# ...
			#% lecture d'une variable
			instructionTable.append('empiler('+str(identifierTable[ident][1])+')')
			instructionTable.append('valeurPile()')
	else:
		logger.error("Unknown Value!")
		raise AnaSynException("Unknown Value!")

def valeur(lexical_analyser):
	if lexical_analyser.isInteger():
		entier = lexical_analyser.acceptInteger()
		instructionTable.append("empiler("+str(entier)+")")
		logger.debug("integer value: " + str(entier))
		return "integer"
	elif lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
		vtype = valBool(lexical_analyser)
		return vtype
	else:
		logger.error("Unknown Value! Expecting an integer or a boolean value!")
		raise AnaSynException("Unknown Value ! Expecting an integer or a boolean value!")

def valBool(lexical_analyser):
	if lexical_analyser.isKeyword("true"):
		lexical_analyser.acceptKeyword("true")
		instructionTable.append("empiler(1)")	
		logger.debug("boolean true value")
				
	else:
		instructionTable.append("empiler(0)")
		logger.debug("boolean false value")
		lexical_analyser.acceptKeyword("false")	
		
	return "boolean"

def es(lexical_analyser):
	logger.debug("parsing E/S instruction: " + lexical_analyser.get_value())
	if lexical_analyser.isKeyword("get"):
		lexical_analyser.acceptKeyword("get")
		lexical_analyser.acceptCharacter("(")
		ident = lexical_analyser.acceptIdentifier()
		lexical_analyser.acceptCharacter(")")
		#% On enregistre dans ident la valeur en entrée
		instructionTable.append('empiler(' + str(identifierTable[ident][1]) + ')')
		instructionTable.append('get()')
		logger.debug("Call to get "+ident)
	elif lexical_analyser.isKeyword("put"):
		lexical_analyser.acceptKeyword("put")
		lexical_analyser.acceptCharacter("(")
		expression(lexical_analyser)
		lexical_analyser.acceptCharacter(")")
		#% On affiche la valeur au sommet de la pile
		instructionTable.append('put()')
		logger.debug("Call to put")
	else:
		logger.error("Unknown E/S instruction!")
		raise AnaSynException("Unknown E/S instruction!")

def boucle(lexical_analyser):
	logger.debug("parsing while loop: ")
	lexical_analyser.acceptKeyword("while")

	
	pileBC.append([None, None]) #% initialisation de (ad1,ad2)
	pileBC[-1][0] = len(instructionTable) #% enregistrement de ad1
	
	expression(lexical_analyser)

	lexical_analyser.acceptKeyword("loop")

	instructionTable.append("tze(ad2-"+str(len(pileBC))+")") #%

	suiteInstr(lexical_analyser)

	instructionTable.append("tra(ad1-"+str(len(pileBC))+")") #% 

	pileBC[-1][1] = len(instructionTable) #%ad2
	lexical_analyser.acceptKeyword("end")
	logger.debug("end of while loop ")
	
	#Remplacement par les adresses #%
	for i in range(len(instructionTable)) :
		x = instructionTable[i]

		if x == "tze(ad2-"+str(len(pileBC))+")":
			instructionTable[i] = "tze(" + str(pileBC[-1][1]) + ")"

		if x == "tra(ad1-"+str(len(pileBC))+")":
			instructionTable[i] = "tra(" + str(pileBC[-1][0]) + ")"
	
	pileBC.pop()#On retire les valeurs de la pile

def altern(lexical_analyser):
	logger.debug("parsing if: ")
	lexical_analyser.acceptKeyword("if")

	expression(lexical_analyser)
	
	lexical_analyser.acceptKeyword("then")

	pileBC.append([None, None]) #% initialisation de (ad1,ad2)
	instructionTable.append("tze(ad1-"+str(len(pileBC))+")") #% 
	
	suiteInstr(lexical_analyser)

	if lexical_analyser.isKeyword("else"):
		lexical_analyser.acceptKeyword("else")
		instructionTable.append("tra(ad2-"+str(len(pileBC))+")") #% 
		pileBC[-1][0] = len(instructionTable) #% ad1 : On devra aller une instruction après la dernière déjà écrite
		
		suiteInstr(lexical_analyser)
	
	if pileBC[-1][0] is None :
		pileBC[-1][0] = len(instructionTable)
	pileBC[-1][1] = len(instructionTable) #% ad2 : On devra aller une instruction après la dernière déjà écrite

	lexical_analyser.acceptKeyword("end")
	logger.debug("end of if")

	#Remplacement par les adresses #%
	for i in range(len(instructionTable)) :
		x = instructionTable[i]

		if x == "tze(ad1-"+str(len(pileBC))+")":
			instructionTable[i] = "tze(" + str(pileBC[-1][0]) + ")"

		if x == "tra(ad2-"+str(len(pileBC))+")":
			instructionTable[i] = "tra(" + str(pileBC[-1][1]) + ")"
	
	pileBC.pop()#On retire les valeurs de la pile



def retour(lexical_analyser):
	logger.debug("parsing return instruction")
	lexical_analyser.acceptKeyword("return")
	expression(lexical_analyser)

	

########################################################################				 	

def mainPy(args):
	filename = args[0] # input 0
	f = None
	try:
		f = open(filename, 'r')
	except:
		print("Error: can\'t open input file!")
		return
		
	outputFilename = args[1] # output 1
	
  	# create logger	  
	LOGGING_LEVEL = logging.INFO
	logger.setLevel(LOGGING_LEVEL)
	ch = logging.StreamHandler()
	ch.setLevel(LOGGING_LEVEL)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	ch.setFormatter(formatter)
	logger.addHandler(ch)

	if False:
		True#
	else:
		False#

	lexical_analyser = an.LexicalAnalyser()

	lineIndex = 0
	for line in f:
		line = line.rstrip('\r\n')
		lexical_analyser.analyse_line(lineIndex, line)
		lineIndex = lineIndex + 1
	f.close()
	
	#print(lexical_analyser.lexical_units) tableau remplis

	# launch the analysis of the program
	lexical_analyser.init_analyser()
	program(lexical_analyser)
		
	if True:
			print("------ IDENTIFIER TABLE ------")
			print(str(identifierTable))
			print("------ END OF IDENTIFIER TABLE ------")

	print("---------------code--------------")
	print(instructionTable)

	if outputFilename != "":
		try:
			output_file = open(outputFilename, 'w')
			for line in instructionTable:
				output_file.write(line+"\n")
			output_file.close()
		except:
			print("Error: can\'t open output file!")
			return
	else:
		output_file = sys.stdout

	print("---------------Execution--------------")

	m.start(outputFilename)


def main():
 	
	parser = argparse.ArgumentParser(description='Do the syntactical analysis of a NNP program.')
	parser.add_argument('inputfile', type=str, nargs=1, help='name of the input source file')
	parser.add_argument('-o', '--outputfile', dest='outputfile', action='store', \
				default="", help='name of the output file (default: stdout)')
	parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
	parser.add_argument('-d', '--debug', action='store_const', const=logging.DEBUG, \
				default=logging.INFO, help='show debugging info on output')
	parser.add_argument('-p', '--pseudo-code', action='store_const', const=True, default=False, \
				help='enables output of pseudo-code instead of assembly code')
	parser.add_argument('--show-ident-table', action='store_true', \
				help='shows the final identifiers table')
	args = parser.parse_args()

	filename = args.inputfile[0]
	f = None
	try:
		f = open(filename, 'r')
	except:
		print("Error: can\'t open input file!")
		return
		
	outputFilename = args.outputfile
	
  	# create logger	  
	LOGGING_LEVEL = args.debug
	logger.setLevel(LOGGING_LEVEL)
	ch = logging.StreamHandler()
	ch.setLevel(LOGGING_LEVEL)
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	ch.setFormatter(formatter)
	logger.addHandler(ch)

	if args.pseudo_code:
		True#
	else:
		False#

	lexical_analyser = an.LexicalAnalyser()

	lineIndex = 0
	for line in f:
		line = line.rstrip('\r\n')
		lexical_analyser.analyse_line(lineIndex, line)
		lineIndex = lineIndex + 1
	f.close()
	
	#print(lexical_analyser.lexical_units) tableau remplis

	# launch the analysis of the program
	lexical_analyser.init_analyser()
	program(lexical_analyser)
		
	if args.show_ident_table:
			print("------ IDENTIFIER TABLE ------")
			print(str(identifierTable))
			print("------ END OF IDENTIFIER TABLE ------")

	print("---------------code--------------")
	print(instructionTable)

	if outputFilename != "":
		try:
			output_file = open(outputFilename, 'w')
			for line in instructionTable:
				output_file.write(line+"\n")
			output_file.close()
		except:
			print("Error: can\'t open output file!")
			return
	else:
		output_file = sys.stdout

	print("---------------Execution--------------")

	m.start(outputFilename)

	# Outputs the generated code to a file
	#instrIndex = 0
	#while instrIndex < codeGenerator.get_instruction_counter():
	#		output_file.write("%s\n" % str(codeGenerator.get_instruction_at_index(instrIndex)))
	#		instrIndex += 1

########################################################################				 

if __name__ == "__main__":
	main()
