#====================================================
# Import
#====================================================

#======================
# Vendors

#

#======================
# Own

import logging

#====================================================
# Class Definition
#====================================================

class tab:
	"""
		Tab class definition
	"""

	##Constructors##
	def __init__(self, VM):
		"""
			Tab class definition
		"""
		self.pointer = 0
		self.content = []
		self.VM = VM

	##S&G##
	#setPointer#
	#getPointer#

	def setPointer(self, pointer):
		"""
			Set the pointer
		"""
		self.pointer = pointer

	def getPointer(self):
		"""
			Get the pointer
		"""
		return self.pointer

	##Methods##
	#moveTo#
	#addContent#

	def moveTo(self, pointer):
		"""
			Move the pointer
		"""
		self.setPointer(pointer)

	def addContent(self, content):
		"""
			add content
		"""

		self.VM.method.setValue(content)
		if self.VM.method.isValide():
			self.content.append(content)
			return True

	def runInstruction(self, pointer):
		"""
			Run the instruction at the pointer
		"""
		name = str(self.content[pointer][0])
		arg = str(self.content[pointer][1])
		m = (name+"("+arg+")")
		eval("self.VM.method." + m)

#====================================================
# End
#====================================================
