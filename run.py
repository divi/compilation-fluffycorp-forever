#====================================================
# Import
#====================================================

#======================
# Vendors

import sys
import os.path
from os import path

#======================
# Own

import src.main as m
import src.anasyn as an

#====================================================
# Define
#====================================================

class COLOR:
    Black= "\u001b[30m"
    Red= "\u001b[31m"
    Green= "\u001b[32m"
    Yellow= "\u001b[33m"
    Blue= "\u001b[34m"
    Magenta= "\u001b[35m"
    Cyan= "\u001b[36m"
    White= "\u001b[37m"
    Reset= "\u001b[0m"


title = """
#================================================================================================#
#                            Compilation Project by the FluffyCorp Team                          #
#================================================================================================#
"""

footer = """
#================================================================================================#
#                                             Have Fun                                           #
#================================================================================================#
"""

#====================================================
# Functions
#====================================================

def makeTitle():
	if (sys.platform == "linux"):
		return COLOR.Magenta+title+COLOR.Magenta
	else:
		return title

def makeUsage():
	if (sys.platform == "linux"):
		return COLOR.Yellow+"usage :"+COLOR.Blue+" python"+COLOR.Green+" run.py"+COLOR.Cyan+" [d] [vm|as] [n]"+COLOR.Reset+"\n"
	else:
		return "usage : python run.py [d] [vm|as] [n]\n"

def makeFooter():
	if (sys.platform == "linux"):
		return COLOR.Magenta+footer+COLOR.Magenta
	else:
		return footer

def displayContent():
	if (sys.platform == "linux"):
		print("\t - "+COLOR.Blue+"python"+COLOR.Reset+" :  Make sure you are using Python3")
		print("\t - "+COLOR.Green+"run.py"+COLOR.Reset+" :  Python program to rules them all")
		print("\t - "+COLOR.Cyan+"d "+COLOR.Reset+"     :  This argument will delete the logs")
		print("\t - "+COLOR.Cyan+"vm"+COLOR.Reset+"     :  Use this parameter if you want only to start the VM")
		print("\t - "+COLOR.Cyan+"as"+COLOR.Reset+"     :  Use this parameter if you want to start the all process")
		print("\t - "+COLOR.Cyan+"n"+COLOR.Reset+"      :  Id of the file to start\n")
		return
	else:
		print("\t - python :  Make sure you are using Python3")
		print("\t - run.py :  Python program to rules them all")
		print("\t - d 	  :  This argument will delete the logs")
		print("\t - vm     :  Use this parameter if you want only to start the VM")
		print("\t - as     :  Use this parameter if you want to start the all process")
		print("\t - n      :  Id of the file to start\n")
		return

def loadingFile(file):
	if (sys.platform == "linux"):
		print("\n"+COLOR.Yellow+"Loading the file : "+COLOR.Green+file+COLOR.Reset)
		if (path.exists(file)):
			print(COLOR.Green+"File Loaded"+COLOR.Reset+"\n")
			return True
		else:
			print(COLOR.Red+"Error : No such file "+file+COLOR.Reset+"\n")
			return False
	else:
		print("\nLoading the file : "+file)
		if (path.exists(file)):
			print("File Loaded"+"\n")
			return True
		else:
			print("Error : No such file "+file)
			return False

#====================================================
# Main
#====================================================

def run(args):
	if (len(args)>=2 and args[1] == "d"):
		for directory in os.listdir("./log"):
			for filename in os.listdir("./log/"+directory):
				os.remove("./log/"+directory+"/"+filename)
			os.rmdir("./log/"+directory) 
		del args[1]

	if len(args)<=1 or (args[1] != "vm" and args[1] != "as") or not args[2].isnumeric():
		print(COLOR.Red+"Error, bad usage"+COLOR.Reset)

		print(makeTitle())
		print(makeUsage())

		displayContent()
		
		print(makeFooter())
		return
	
	if (args[1] == "vm"):
		if loadingFile("./tests/assembleur/assembly"+args[2]+".as"):
			m.start("./tests/assembleur/assembly"+args[2]+".as")
		else:
			return
	elif(args[1] == "as"):
		if loadingFile("./tests/nna/correct"+args[2]+".nno"):
			an.mainPy(["./tests/nna/correct"+args[2]+".nno", "./tests/out/correct"+args[2]+".syn"])
		else:
			return

#====================================================
# Entry Point
#====================================================

if __name__ == "__main__":
	run(sys.argv)

#====================================================
# End
#====================================================